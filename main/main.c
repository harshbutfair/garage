#include "config.h"      /* contains wifi parameters */

#include <stdio.h>
#include <string.h>
#include "sdkconfig.h"
#include <freertos/FreeRTOS.h>
#include <freertos/event_groups.h>
#include <esp_log.h>
#include <esp_wifi.h>
#include <nvs_flash.h>

#include <driver/adc.h>
#include <esp_event.h>
#include <esp_wifi.h>
#include <lwip/netdb.h>

static const char *TAG = "garage";

static const int NUM_SAMPLES = 200;  // higher -> more accurate (but slower)
static const int THRESHOLD = 8; // typically ~27 when open, 2 or 3 when closed
static const int CLOSED_SAMPLES = 5;

/* FreeRTOS event group to signal when we are connected*/
static EventGroupHandle_t s_wifi_event_group;

typedef struct Message
{
    int32_t doorIsOpen;
    int32_t secondsSinceChange;
    int32_t sensorReading;
} Message;

/* The event group allows multiple bits for each event, but we only care about one event:
 * - we are connected to the AP with an IP
 */
#define WIFI_CONNECTED_BIT BIT0

static void wifi_init_sta();

static int read_sensor();


void app_main(void)
{
    wifi_init_sta();

    // Construct destination address.
    struct sockaddr_in dest;
    dest.sin_addr.s_addr = inet_addr("192.168.0.3");
    dest.sin_family = AF_INET;
    dest.sin_port = htons(0xd00); // "door", 3328

    int sock = socket(AF_INET, SOCK_DGRAM, IPPROTO_IP);
    if( sock < 0 )
    {
        // Now what?
        ESP_LOGI(TAG, "Error opening socket. All bets are off.");
    }

    // The ADC1 module must be enabled by calling adc1_config_width() before
    // calling hall_sensor_read(). ADC1 should be configured for 12 bit
    // readings, as the hall sensor readings are low values and do not cover
    // the full range of the ADC.
    adc1_config_width(ADC_WIDTH_BIT_12);

    // Initialise state
    time_t now = time(NULL);
    time_t stateChangeTime = now;
    bool wasOpen = false;

    // Get 5 initial readings on startup, with the assumption that the door is
    // closed. (If the door is open, it will sort it self out after the door is
    // closed).
    int closedReadings[CLOSED_SAMPLES];
    for( int i = 0; i < CLOSED_SAMPLES; ++i )
    {
        closedReadings[i] = read_sensor();
        ESP_LOGI(TAG, "Initial reading %d was %d", i+1, closedReadings[i]);
        sleep(1);
    }

    // Threshold for transition from open to closed. As we assume the door is
    // closed at startup, this doesn't need an initial value.
    int closedThreshold = 0;

    while(true)
    {
        // Get current time
        time(&now);

        int currentReading = read_sensor();

        // Calculate average of last five readings taken while the door was closed. 
        int closedAverage = 0;
        for( int i = 0; i < CLOSED_SAMPLES; ++i )
        {
            closedAverage += closedReadings[i];
        }
        closedAverage = closedAverage / CLOSED_SAMPLES;
        int openThreshold = abs(closedAverage) + THRESHOLD;

        bool isOpen = wasOpen;
        if( wasOpen )
        {
            // Transition from open to closed if the current reading exceeds the
            // threshold for being considered closed. That 
            isOpen = abs(currentReading) > closedThreshold;
        }
        else
        {
            // Previously door was closed.  If the new reading exceeds the average
            // closed reading by more than the threshold then the door is now
            // believed to be open.
            isOpen = abs(currentReading - closedAverage) >= THRESHOLD;
        }

        if( isOpen )
        {
            // Update threshold for door to be considered closed.
            // Make it closer to the closed reading because that's based
            // on multiple readings so should be more reliable.
            closedThreshold = abs(closedAverage * 2 + currentReading ) / 3;
        }

        if( !isOpen )
        {
            // Update historical readings with door closed.
            for(int i = 0; i < (CLOSED_SAMPLES-1); ++i)
            {
                closedReadings[i] = closedReadings[i+1];
            }
            closedReadings[CLOSED_SAMPLES-1] = currentReading;
        }

        if( isOpen != wasOpen )
        {
            stateChangeTime = now;
            wasOpen = isOpen;
            ESP_LOGI(TAG, "Door is now %s!", isOpen ? "open" : "closed" );
        }

        ESP_LOGI(TAG, "Current reading: %3d, State: %5s, Threshold: %c%d",
                currentReading,
                isOpen ? "open" : "closed",
                isOpen ? '<' : '>',
                isOpen ? closedThreshold : openThreshold);

        Message msg;
        msg.doorIsOpen = ( isOpen ? 1 : 0 );
        msg.secondsSinceChange = now - stateChangeTime;
        msg.sensorReading = currentReading;

        // Send message if door is open, or if door was recently closed.
        if( isOpen || msg.secondsSinceChange < 30 )
        {
            ESP_LOGI(TAG, "Door has been %s for %d seconds",
                            isOpen ? "open" : "closed",
                            msg.secondsSinceChange);

            ssize_t res = sendto(sock, &msg, sizeof(msg), 0,
                    (const struct sockaddr*)&dest, sizeof(dest));
            ESP_LOGD(TAG, "Sent %d bytes", res);
        }

        sleep(5);
    }

    vTaskDelete(NULL);
}

static int read_sensor()
{
    // Perform many samples to average out noise
    int count = 0;
    for( int i = 0; i < NUM_SAMPLES; ++i )
    {
        count += hall_sensor_read();
    }
    return count / NUM_SAMPLES;
}

static void event_handler(void* arg, esp_event_base_t event_base,
                                int32_t event_id, void* event_data)
{
    if (event_base == WIFI_EVENT && event_id == WIFI_EVENT_STA_START)
    {
        esp_wifi_connect();
    }
    else if (event_base == WIFI_EVENT && event_id == WIFI_EVENT_STA_DISCONNECTED)
    {
        esp_wifi_connect();
        ESP_LOGI(TAG, "Retry to connect to the AP");
    }
    else if (event_base == IP_EVENT && event_id == IP_EVENT_STA_GOT_IP)
    {
        xEventGroupSetBits(s_wifi_event_group, WIFI_CONNECTED_BIT);
    }
}

static void disconnected_handler(void* arg, esp_event_base_t event_base,
                                int32_t event_id, void* event_data)
{
    ESP_LOGI(TAG, "WiFi Disconnected - REBOOTING");
    esp_restart();
}

static void wifi_init_sta(void)
{
    //Initialize NVS
    ESP_LOGI(TAG, "Initialising NVS (required for wifi)");
    esp_err_t ret = nvs_flash_init();
    if (ret == ESP_ERR_NVS_NO_FREE_PAGES || ret == ESP_ERR_NVS_NEW_VERSION_FOUND)
    {
        ESP_LOGI(TAG, "Full reinitialisation of NVS required");
        ESP_ERROR_CHECK(nvs_flash_erase());
        ret = nvs_flash_init();
    }
    ESP_ERROR_CHECK(ret);

    ESP_LOGI(TAG, "Initialising wifi with static IP 192.168.0.72");
    s_wifi_event_group = xEventGroupCreate();

    ESP_ERROR_CHECK(esp_netif_init());

    ESP_ERROR_CHECK(esp_event_loop_create_default());
    esp_netif_t* netif = esp_netif_create_default_wifi_sta();
    ESP_ERROR_CHECK( esp_netif_dhcpc_stop( netif ) );

    esp_netif_ip_info_t ipinfo;
    esp_netif_set_ip4_addr( &ipinfo.ip, 192, 168, 0, 72 );
    esp_netif_set_ip4_addr( &ipinfo.netmask, 255, 255, 255, 0 );
    esp_netif_set_ip4_addr( &ipinfo.gw, 0, 0, 0, 0 );

    ESP_ERROR_CHECK( esp_netif_set_ip_info( netif, &ipinfo ) );

    wifi_init_config_t cfg = WIFI_INIT_CONFIG_DEFAULT();
    ESP_ERROR_CHECK(esp_wifi_init(&cfg));

    esp_event_handler_instance_t instance_any_id;
    esp_event_handler_instance_t instance_got_ip;
    ESP_ERROR_CHECK(esp_event_handler_instance_register(WIFI_EVENT,
                                                        ESP_EVENT_ANY_ID,
                                                        &event_handler,
                                                        NULL,
                                                        &instance_any_id));
    ESP_ERROR_CHECK(esp_event_handler_instance_register(IP_EVENT,
                                                        IP_EVENT_STA_GOT_IP,
                                                        &event_handler,
                                                        NULL,
                                                        &instance_got_ip));

    wifi_config_t wifi_config = {
        .sta = {
            .ssid = WIFI_SSID,
            .password = WIFI_PASSWORD,
            .pmf_cfg = {
                .capable = true,
                .required = false
            },
        },
    };
    ESP_ERROR_CHECK(esp_wifi_set_mode(WIFI_MODE_STA) );
    ESP_ERROR_CHECK(esp_wifi_set_config(ESP_IF_WIFI_STA, &wifi_config) );
    ESP_LOGI(TAG, "Starting wifi");
    ESP_ERROR_CHECK(esp_wifi_start() );

    /* Waiting until the connection is established (WIFI_CONNECTED_BIT). Retry forever!
     * The bits are set by event_handler() (see above) */
    EventBits_t bits = xEventGroupWaitBits(s_wifi_event_group,
            WIFI_CONNECTED_BIT,
            pdFALSE,
            pdFALSE,
            portMAX_DELAY);

    /* xEventGroupWaitBits() returns the bits before the call returned, hence we can test which event actually
     * happened. */
    if (bits & WIFI_CONNECTED_BIT)
    {
        ESP_LOGI(TAG, "Connected to access point %s", WIFI_SSID );
    }
    else
    {
        ESP_LOGI(TAG, "Unexpected event from wifi");
    }

    /* The event will not be processed after unregister */
    ESP_ERROR_CHECK(esp_event_handler_instance_unregister(IP_EVENT, IP_EVENT_STA_GOT_IP, instance_got_ip));
    ESP_ERROR_CHECK(esp_event_handler_instance_unregister(WIFI_EVENT, ESP_EVENT_ANY_ID, instance_any_id));
    vEventGroupDelete(s_wifi_event_group);

    /* Register for notification in case wifi connection is lost */
    ESP_ERROR_CHECK(esp_event_handler_instance_register(WIFI_EVENT,
                                                        WIFI_EVENT_STA_DISCONNECTED,
                                                        &disconnected_handler,
                                                        NULL,
                                                        &instance_any_id));  // discarded
}

