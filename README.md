# Garage Door Monitor

An ESP-32 is positioned near the garage door, and is close to a magnet
when the door is open. The ESP-32 reads the current magnetic field using
its onboard Hall effect sensor every 5 seconds.

The ESP-32 sends a UDP message to the server when the door is open, or
when the door has been closed for less than 30 seconds.

A receiver on the server sends a message via signal when the door has
been open for more than a specified time threshold, and then again when
the door has been closed.

## Configuration
* main/config.h is not checked in, it contains wifi ssid and password
* garage\_receiver/garage.toml is not checked in, it contains phone number
  and API key

## Hardware
* Espressif ESP32-PICO-KIT (any ESP32 will do)

# Building and running
* cd ../esp-idf
* source export.sh
* idf.py build
* idf.py flash
* idf.py monitor
