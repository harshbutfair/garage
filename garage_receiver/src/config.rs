use serde::Deserialize;

#[derive(Deserialize)]
pub struct Config {
    pub listen_port: u16,
    pub alert: Vec<AlertConfig>,
}

#[derive(Deserialize)]
pub struct AlertConfig {
    pub open_seconds: u32,
    pub api_key: u32,
    pub number: String,
    pub open_text: String,
    pub closed_text: String,
}
