use crate::config::AlertConfig;
use ureq::Error;

pub struct Alert {
    open_seconds: u32,
    api_key: u32,
    number: String,
    alert_sent: bool,
    open_text: String,
    closed_text: String,
}

const BASE_URL: &str = "https://api.callmebot.com/signal/send.php";

impl Alert {
    pub fn new(config: &AlertConfig) -> Self {
        Self {
            open_seconds: config.open_seconds,
            number: config.number.clone(),
            api_key: config.api_key,
            alert_sent: false,
            open_text: config.open_text.clone(),
            closed_text: config.closed_text.clone(),
        }
    }

    pub fn door_open(&mut self, seconds: u32) {
        if seconds >= self.open_seconds
            && !self.alert_sent
            && Self::send(&self.number, self.api_key, &self.open_text).is_ok()
        {
            self.alert_sent = true;
        }
    }

    pub fn door_closed(&mut self) {
        if self.alert_sent && Self::send(&self.number, self.api_key, &self.closed_text).is_ok() {
            self.alert_sent = false;
        }
    }

    fn send(number: &str, api_key: u32, message: &str) -> Result<(), ()> {
        let url = BASE_URL.to_owned()
            + "?phone="
            + number
            + "&apikey="
            + &api_key.to_string()
            + "&text="
            + urlencoding::encode(message).as_ref();
        match ureq::get(&url)
            .timeout(std::time::Duration::from_secs(20))
            .call()
        {
            Ok(_) => Ok(()),
            Err(Error::Status(code, _response)) => {
                error!("Sending to callmebot returned HTTP error {}", code);
                Err(())
            }
            Err(e) => {
                error!("Sending to callmebot failed: {}", e);
                Err(())
            }
        }
    }
}
