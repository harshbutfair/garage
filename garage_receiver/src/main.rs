#![warn(clippy::nursery)]
#![warn(clippy::pedantic)]
#![allow(clippy::module_name_repetitions)]

#[macro_use]
extern crate log;

mod alert;
mod config;

use alert::Alert;
use config::Config;
use std::net::{Ipv4Addr, SocketAddrV4, UdpSocket};
use std::path::Path;
use std::sync::atomic::{AtomicBool, Ordering};
use std::sync::Arc;
use std::time::Duration;

const CONFIG_FILE: &str = "garage.toml";
const EXIT_FAILURE: i32 = 1;

const BUFFER_SIZE: usize = 64;
const MESSAGE_SIZE: usize = 12;

// MESSAGE FORMAT
// offset 0  Is door currently open - 4 bytes
// offset 4  Seconds since door opened or closed - 4 bytes
// offset 8  Current sensor reading - 4 bytes
struct Message {
    is_open: bool,
    time_since: u32,
    sensor_reading: i32,
}

fn main() {
    // Setup logging
    let formatter = syslog::Formatter3164 {
        facility: syslog::Facility::LOG_USER,
        hostname: None,
        process: "garage_receiver".into(),
        pid: std::process::id(),
    };

    let logger =
        syslog::udp(formatter, "0.0.0.0:0", "192.168.0.3:514").expect("Failed to start syslog");
    log::set_boxed_logger(Box::new(syslog::BasicLogger::new(logger)))
        .map(|()| log::set_max_level(log::LevelFilter::Debug))
        .expect("Failed to set logger");

    let config_path = Path::new(CONFIG_FILE);
    if !config_path.exists() {
        if let Ok(path) = std::env::current_exe() {
            if let Some(path) = path.parent() {
                std::env::set_current_dir(path).unwrap();
            }
        }
    }

    // Read config file into string.
    let config_str = match std::fs::read_to_string(config_path) {
        Ok(s) => s,
        Err(e) => {
            error!("Failed to read config file: {}", e);
            std::process::exit(EXIT_FAILURE);
        }
    };

    // Parse config file.
    let config = match toml::from_str::<Config>(&config_str) {
        Ok(conf) => conf,
        Err(e) => {
            error!("Config file parsing failed: {}", e);
            std::process::exit(EXIT_FAILURE);
        }
    };

    let mut alerts: Vec<Alert> = Vec::new();
    for alert_config in config.alert {
        alerts.push(Alert::new(&alert_config));
    }

    // Start ctrl-c handler.
    let keep_running = Arc::new(AtomicBool::new(true));
    let keep_running_clone = keep_running.clone();
    if let Err(e) = ctrlc::set_handler(move || {
        keep_running_clone.store(false, Ordering::SeqCst);
    }) {
        error!("Error installing ctrl-c handler: {}", e);
    }

    let addr = SocketAddrV4::new(Ipv4Addr::new(0, 0, 0, 0), config.listen_port);
    let socket = UdpSocket::bind(addr).map_err(|e| e.to_string()).unwrap();

    socket
        .set_nonblocking(true)
        .map_err(|e| e.to_string())
        .unwrap();

    let mut door_open = false;
    while keep_running.load(Ordering::SeqCst) {
        if let Some(msg) = receive(&socket) {
            if door_open != msg.is_open {
                door_open = msg.is_open;
                info!(
                    "Garage door is {}",
                    if door_open { "open" } else { "closed" }
                );
            }

            for alert in &mut alerts {
                if door_open {
                    alert.door_open(msg.time_since);
                } else {
                    alert.door_closed();
                }
            }
        }
        std::thread::sleep(Duration::from_millis(100));
    }
}

fn receive(socket: &UdpSocket) -> Option<Message> {
    let mut received = None;

    // Keep reading until there are no messages remaining. Return the
    // last valid message.
    let mut receive_ok = true;
    let mut buffer = [0; BUFFER_SIZE];

    while receive_ok {
        match socket.recv_from(&mut buffer) {
            Ok((num_bytes, src)) if num_bytes == MESSAGE_SIZE => {
                debug!("Received {} bytes from {}", num_bytes, src);

                if received.is_some() {
                    debug!("Newer message available, previous message skipped");
                }

                let msg = Message {
                    is_open: get_u32_le(&buffer, 0) != 0,
                    time_since: get_u32_le(&buffer, 4),
                    sensor_reading: get_i32_le(&buffer, 8),
                };

                debug!(
                    "Garage door has been {} for {} seconds (sensor reading {})",
                    if msg.is_open { "open" } else { "closed" },
                    msg.time_since,
                    msg.sensor_reading
                );

                received = Some(msg);
            }
            Ok((num_bytes, _src)) if num_bytes == 1 => {
                // Ignore, this is most likely a monit connection check
            }
            Ok((num_bytes, src)) => {
                warn!(
                    "Received {} bytes from {}, expected {}",
                    num_bytes, src, MESSAGE_SIZE
                );
            }
            Err(e) => {
                receive_ok = false;
                if e.kind() != std::io::ErrorKind::WouldBlock {
                    error!("UDP receive error: {}", e);
                }
            }
        }
    }

    received
}

// Get little-endian u32 from specified offset of buffer.
fn get_u32_le(buffer: &[u8], offset: usize) -> u32 {
    u32::from(buffer[offset + 3]) << 24
        | u32::from(buffer[offset + 2]) << 16
        | u32::from(buffer[offset + 1]) << 8
        | u32::from(buffer[offset])
}

fn get_i32_le(buffer: &[u8], offset: usize) -> i32 {
    //let array = [ buffer[offset], buffer[offset+1], buffer[offset+2], buffer[offset+3] ];
    //i32::from_le_bytes(array)
    i32::from(buffer[offset + 3]) << 24
        | i32::from(buffer[offset + 2]) << 16
        | i32::from(buffer[offset + 1]) << 8
        | i32::from(buffer[offset])
}
